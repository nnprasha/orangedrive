package com.example.nikhil.orangedrive.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.nikhil.orangedrive.Adapter.MyFirebaseRecyclerAdapter;
import com.example.nikhil.orangedrive.Data.JobsData;
import com.example.nikhil.orangedrive.Interface.CompanyApplicantsListener;
import com.example.nikhil.orangedrive.Interface.StudentJobApplicationsListener;
import com.example.nikhil.orangedrive.Interface.onRecyclerViewItemClickListener;
import com.example.nikhil.orangedrive.Models.Jobs;
import com.example.nikhil.orangedrive.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;

import java.util.HashMap;

import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.FlipInBottomXAnimator;

/**
 * Created by Nikhil on 12/4/2017.
 */

public class StudentJobApplicationFragment extends Fragment {

    StudentJobApplicationsListener mListener;
    onRecyclerViewItemClickListener mListener2;

    RecyclerView mRecyclerView;
    RecyclerView.LayoutManager mLayoutManager;
    ImageView signout;
    //MyRecyclerViewAdapter mRecyclerViewAdapter;

    MyFirebaseRecyclerAdapter myFirebaseRecyclerAdapter;
    String student_node = "Students";

    JobsData movieData;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_studentjobapplications,container,false);
        try{
            mListener2=(onRecyclerViewItemClickListener) getContext();
            mListener=(StudentJobApplicationsListener) getContext();
        }catch(ClassCastException e){
            throw new ClassCastException("The hosting activity doesn't implement the interface");
        }

        mListener.setToolbarTitle("My Job Applications");

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference childRef= FirebaseDatabase.getInstance().getReference().child(student_node)
                .child(user.getUid()).child("jobapplications").getRef();
        movieData=new JobsData(childRef);

        mRecyclerView = rootView.findViewById(R.id.studentjobapplicationlist);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager=new LinearLayoutManager(getActivity());

        mRecyclerView.setLayoutManager(mLayoutManager);



        myFirebaseRecyclerAdapter=new MyFirebaseRecyclerAdapter(Jobs.class, R.layout.layout_cardview,
                MyFirebaseRecyclerAdapter.MovieViewHolder.class, childRef, getContext());

        //Set adapter animation
        mRecyclerView.setAdapter(new ScaleInAnimationAdapter(myFirebaseRecyclerAdapter));
        //mRecyclerView.setAdapter(new AlphaInAnimationAdapter(myFirebaseRecyclerAdapter));
        if (movieData.getSize() == 0) {
            movieData.setAdapter(myFirebaseRecyclerAdapter);
            //getApplicationContext()-activity is used movieData.initializeDataFromCloud();
            movieData.setContext(getActivity());
            movieData.initializeDataFromCloud();
        }
        //defaultAnimation();

        //setting item animation
        itemAnimation();

        myFirebaseRecyclerAdapter.setOnItemClickListener(new MyFirebaseRecyclerAdapter.OnRecyclerViewItemClickListener(){

            @Override
            public void OnItemClick(final View v, int position) {
                HashMap<String, ?> movie = (HashMap<String, ?>) movieData.getItem(position);
                String id = (String) movie.get("CompanyId") + "_" + movie.get("JobId");
                DatabaseReference ref = movieData.getFireVaseRef();
                ref.child(id).addListenerForSingleValueEvent(new com.google.firebase.database.ValueEventListener(){

                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        HashMap<String, String> movie =  (HashMap<String, String>) dataSnapshot.getValue();
                        mListener2.loadMovieDetailFragment(movie, v.findViewById(R.id.cardImage));
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.d("My Test", "The read failed: " + databaseError.getMessage());
                    }
                });
            }
        });
        return rootView;
    }
    public void itemAnimation()
    {
        FlipInBottomXAnimator animator = new FlipInBottomXAnimator();
        animator.setAddDuration(500);
        mRecyclerView.setItemAnimator(animator);
//        FadeInLeftAnimator animator=new FadeInLeftAnimator();
//        animator.setAddDuration(100);
//        animator.setRemoveDuration(100);
//        mRecyclerView.setItemAnimator(animator);

    }
    public static StudentJobApplicationFragment newInstance(){
        return new StudentJobApplicationFragment();
    }
}
