package com.example.nikhil.orangedrive.Interface;

/**
 * Created by Nikhil on 11/27/2017.
 */

public interface StudentLoginFragmentListener {
    void setToolBarTitle(String name);
    void onLoadRegisterFragment();
    void onLoadODSActivity();
    void backPressed();
}
