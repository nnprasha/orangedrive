package com.example.nikhil.orangedrive;

/**
 * Created by Nikhil on 12/7/2017.
 */
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.nikhil.orangedrive.Activity.WelcomeActivity;


public class Startup extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction() != null) {
            if( intent.getAction().equals(Intent.ACTION_USER_PRESENT)) {
                Intent s = new Intent(context, WelcomeActivity.class);
                s.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);


                context.startActivity(s);
            }
        }


    }
}