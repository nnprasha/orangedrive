package com.example.nikhil.orangedrive.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.nikhil.orangedrive.Fragment.RegisterUserFragment;
import com.example.nikhil.orangedrive.Fragment.StudentLoginFragment;
import com.example.nikhil.orangedrive.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.iid.FirebaseInstanceId;
import com.example.nikhil.orangedrive.Interface.StudentLoginFragmentListener;

/**
 * Created by Nikhil on 11/26/2017.
 */

public class StudentLoginActivity extends AppCompatActivity implements StudentLoginFragmentListener {


    Toolbar toolbar;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    public ProgressDialog mProgressDialog;

    private static final String TAG = "StudentLoginActivity";

    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_studentlogin);

        setUpToolBar();

        if(savedInstanceState == null)
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.studentloginpagecontainer, StudentLoginFragment.newInstance())
                    .commit();
    }

    private void setUpToolBar(){
        toolbar=(Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setTitleTextColor(getColor(R.color.colorWhite));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setToolBarTitle(String name) {
        this.toolbar.setTitle(name);
        this.toolbar.setTitleTextColor(getColor(R.color.colorWhite));
    }

    @Override
    public void onLoadRegisterFragment() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.studentloginpagecontainer, RegisterUserFragment.newInstance())
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onLoadODSActivity() {
        Intent intent = new Intent(StudentLoginActivity.this, OrangeDriveStudentActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.from_middle, R.anim.to_middle);
    }

    @Override
    public void backPressed() {
        onBackPressed();
    }
}
