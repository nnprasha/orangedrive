package com.example.nikhil.orangedrive.Interface;

/**
 * Created by Nikhil on 11/28/2017.
 */

public interface ProfileEditListener {
    void setToolBarTitle(String name);
    void backPressed();
    void updateSuccessful();
}
