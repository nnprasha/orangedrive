package com.example.nikhil.orangedrive.Fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.nikhil.orangedrive.Activity.OrangeDriveCompanyActivity;
import com.example.nikhil.orangedrive.Activity.OrangeDriveStudentActivity;
import com.example.nikhil.orangedrive.Interface.JobDetailViewListener;
import com.example.nikhil.orangedrive.Interface.MyApplicantsDetailViewListener;
import com.example.nikhil.orangedrive.R;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.HashMap;

/**
 * Created by Nikhil on 12/5/2017.
 */

public class MyApplicantDetailViewFragment extends Fragment implements View.OnClickListener {

    HashMap<String, ?> movie;
    FirebaseStorage storage;
    StorageReference storageRef;
    MyApplicantsDetailViewListener mListener;

    public static String profilepictures="profileimages/";

    TextView mApplied;
    TextView mAddress;
    TextView mCgpa;
    TextView mEmail;
    TextView phno;
    TextView resume;
    ImageView profileImg;
    Button mViewResume;

    String resumeurl=null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        storage = FirebaseStorage.getInstance();

        if(getArguments()!=null)
        {
            movie=(HashMap<String,?>) getArguments().getSerializable("Detail");
        }

        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_myapplicantdetailview,container,false);

        try{
            mListener=(MyApplicantsDetailViewListener) getContext();
        }catch(ClassCastException e){
            throw new ClassCastException("The hosting activity doesn't implement the interface");
        }

        Toolbar toolbar = rootView.findViewById(R.id.flexible_example_toolbar);
        toolbar.setNavigationOnClickListener(new Toolbar.OnClickListener(){

            @Override
            public void onClick(View v) {
                mListener.pressedBackButton();
            }

        });

        toolbar.setTitle(movie.get("name").toString());
        //views
        mApplied = rootView.findViewById(R.id.appliedfor);
        mAddress = rootView.findViewById(R.id.studentaddress);
        mCgpa = rootView.findViewById(R.id.cgpa);
        mEmail = rootView.findViewById(R.id.studentemail);
        phno = rootView.findViewById(R.id.studentphno);
        resume = rootView.findViewById(R.id.viewresume);
        profileImg = rootView.findViewById(R.id.profileimage);
        mViewResume = rootView.findViewById(R.id.viewresume);

        mApplied.setText(movie.get("appliedfor").toString());
        mAddress.setText(movie.get("address").toString());
        mCgpa.setText(movie.get("cgpa").toString());
        mEmail.setText(movie.get("netid").toString());
        phno.setText(movie.get("phno").toString());
        //resume.setText(movie.get("resumeurl").toString());
        resumeurl = movie.get("resumeurl").toString();

        mViewResume.setOnClickListener(this);

        storageRef = storage.getReference().child(profilepictures+movie.get("profileurl").toString());

        Glide.with(getContext())
                .using(new FirebaseImageLoader())
                .load(storageRef)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(profileImg);
        profileImg.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorTint), android.graphics.PorterDuff.Mode.MULTIPLY);

        profileImg.setTransitionName(movie.get("id").toString());

        return rootView;
    }

    public static MyApplicantDetailViewFragment newInstance(HashMap<String, ?> movie){
        MyApplicantDetailViewFragment fragment = new MyApplicantDetailViewFragment();
        Bundle args=new Bundle();
        args.putSerializable("Detail",movie);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((OrangeDriveCompanyActivity)getActivity()).getSupportActionBar().hide();
    }
    @Override
    public void onStop() {
        super.onStop();
        ((OrangeDriveCompanyActivity)getActivity()).getSupportActionBar().show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.viewresume:
                Uri uri = Uri.parse(resumeurl); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
        }
    }
}
