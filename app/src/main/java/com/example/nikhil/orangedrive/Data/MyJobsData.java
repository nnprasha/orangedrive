package com.example.nikhil.orangedrive.Data;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.example.nikhil.orangedrive.Adapter.MyFirebaseRecyclerAdapter;
import com.example.nikhil.orangedrive.Adapter.MyJobsRecyclerAdapter;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Nikhil on 12/1/2017.
 */

public class MyJobsData {
    List<Map<String,?>> moviesList;
    List<String> jobsToRetrieve;
    DatabaseReference mRef;
    MyJobsRecyclerAdapter myFirebaseRecyclerAdapter;
    Context mContext;


    public MyJobsData(DatabaseReference childRef){
        moviesList = new ArrayList<Map<String,?>>();
        mRef = childRef;
        myFirebaseRecyclerAdapter = null;
        mContext = null;

    }

    public void setAdapter(MyJobsRecyclerAdapter mAdapter){
        myFirebaseRecyclerAdapter=mAdapter;
    }

    public void removeItemFromServer(Map<String, ?> movie){
        if(movie!=null){
            String id=(String)movie.get("Id");
            mRef.child(id).removeValue();
        }
    }

    public void addItemToServer(Map<String,?> movie){
        if(movie!=null){
            String id = (String) movie.get("Id");
            mRef.child(id).setValue(movie);
        }
    }

    public DatabaseReference getFireVaseRef(){return mRef;}
    public void setContext(Context context){mContext=context;}

    public List<Map<String, ?>> getMoviesList() {
        return moviesList;
    }

    public int getSize(){
        return moviesList.size();
    }

    public HashMap getItem(int i){
        if (i >=0 && i < moviesList.size()){
            return (HashMap) moviesList.get(i);
        } else return null;
    }

    public void onItemRemovedFromCloud(HashMap item){
        int position = -1;
        String id=(String)item.get("JobId");
        for(int i=0;i<moviesList.size();i++){
            HashMap movie = (HashMap)moviesList.get(i);
            String mid = (String)movie.get("JobId");
            if(mid.equals(id)){
                position= i;
                break;
            }
        }
        if(position != -1){
            moviesList.remove(position);
            Toast.makeText(mContext, "Item Removed:" + id, Toast.LENGTH_SHORT).show();
        }
    }

    //Updating local movie list
    public void onItemAddedToCloud(HashMap item){
        int insertPosition = moviesList.size();
        moviesList.add(insertPosition,item);
    }

    public void onItemUpdatedToCloud(HashMap item){
        String id=(String)item.get("JobId");
        for(int i=0;i<moviesList.size();i++){
            HashMap movie = (HashMap)moviesList.get(i);
            String mid = (String)movie.get("JobId");
            if(mid.equals(id)){
                moviesList.remove(i);
                moviesList.add(i,item);
                Log.d("My Test: NotifyChanged",id);
                break;
            }
        }

    }

    public void initializeDataFromCloud() {
        moviesList.clear();
        mRef.addChildEventListener(new com.google.firebase.database.ChildEventListener() {
            @Override
            public void onChildAdded(com.google.firebase.database.DataSnapshot dataSnapshot, String s) {
                Log.d("MyTest: OnChildAdded", dataSnapshot.toString());
                HashMap<String,String> movie = (HashMap<String,String>)dataSnapshot.getValue();
                onItemAddedToCloud(movie);
            }

            @Override
            public void onChildChanged(com.google.firebase.database.DataSnapshot dataSnapshot, String s) {
                Log.d("MyTest: OnChildChanged", dataSnapshot.toString());
                HashMap<String,String> movie = (HashMap<String,String>)dataSnapshot.getValue();
                onItemUpdatedToCloud(movie);
            }

            @Override
            public void onChildRemoved(com.google.firebase.database.DataSnapshot dataSnapshot) {
                Log.d("MyTest: OnChildRemoved", dataSnapshot.toString());
                HashMap<String,String> movie = (HashMap<String,String>)dataSnapshot.getValue();
                onItemRemovedFromCloud(movie);
            }

            @Override
            public void onChildMoved(com.google.firebase.database.DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
}
