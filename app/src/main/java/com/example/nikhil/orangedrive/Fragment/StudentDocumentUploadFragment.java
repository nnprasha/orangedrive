package com.example.nikhil.orangedrive.Fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.nikhil.orangedrive.R;
import com.example.nikhil.orangedrive.Utility.Utility;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;

/**
 * Created by Nikhil on 12/5/2017.
 */

public class StudentDocumentUploadFragment extends Fragment implements View.OnClickListener {

    private String userChoosenTask;
    private static int REQUEST_CAMERA = 1;
    private static int SELECT_FILE = 2;

    LinearLayout mResumeLL;
    EditText addResume;
    Button mResumeUpload;

    public ProgressDialog mProgressDialog;
    Uri file=null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mRootView = inflater.inflate(R.layout.fragment_studentuploaddocuments,container,false);


        mResumeLL = mRootView.findViewById(R.id.resumelinearlayour);
        mResumeLL.setOnClickListener(this);

        addResume = mRootView.findViewById(R.id.add_resume);

        mResumeUpload = mRootView.findViewById(R.id.uploadresume_button);
        mResumeUpload.setOnClickListener(this);



        return mRootView;
    }

    private void selectImage() {
        final CharSequence[] items = { "Choose from Library", "Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Add Documents!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result= Utility.checkPermission(getContext());
                if (items[item].equals("Choose from Library")) {
                    userChoosenTask="Choose from Library";
                    if(result)
                        galleryIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void galleryIntent()
    {
        Intent intent = new Intent();
        intent.setType("application/pdf");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                     if(userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
        }
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        if (data != null) {

                file=data.getData();
                String fileName = getRealPathFromURI(file);


                Log.d("yayayayya",fileName);
                addResume.setText(fileName);
        }
        //mProfileImage.setImageBitmap(bm);
        //ivImage.setImageBitmap(bm);
        //UploadNewProfileImageToFirebase(bm);
    }

    public void UploadResume(){
        if(file!=null){
            showProgressDialog();
            new Handler().postDelayed(new Runnable() {
                  @Override
                  public void run() {
                      String fileName = getRealPathFromURI(file);
                      FirebaseStorage storageRef = FirebaseStorage.getInstance();

                      final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                      StorageReference riversRef = storageRef.getReference().child("studentdocuments/"+user.getUid()+"/"+fileName);
                      UploadTask uploadTask = riversRef.putFile(file);

                      // Register observers to listen for when the download is done or if it fails
                      uploadTask.addOnFailureListener(new OnFailureListener() {
                          @Override
                          public void onFailure(@NonNull Exception exception) {
                              // Handle unsuccessful uploads
                          }
                      }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                          @Override
                          public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                              // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                              Uri downloadUrl = taskSnapshot.getDownloadUrl();

                              DatabaseReference childRef= FirebaseDatabase.getInstance().getReference().child("Students")
                                      .child(user.getUid()).getRef();
                              childRef.child("resumeurl").setValue(downloadUrl.toString());
                              Log.d("uri: ",downloadUrl.toString());
                          }
                      });

                      hideProgressDialog();
                      Toast.makeText(getContext(),"Resume Uploaded", Toast.LENGTH_SHORT).show();
                  }
              }, 500);
        }else{
             Toast.makeText(getContext(),"Select resume", Toast.LENGTH_SHORT).show();
        }

    }

    public String getRealPathFromURI(Uri uri) {
    // can post image

        Uri returnUri = uri;
        Cursor returnCursor =
                getContext().getContentResolver().query(returnUri, null, null, null, null);
    /*
     * Get the column indexes of the data in the Cursor,
     * move to the first row in the Cursor, get the data,
     * and display it.
     */
        int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
        returnCursor.moveToFirst();
        String x = returnCursor.getString(nameIndex);
        return x;
    }

    public static StudentDocumentUploadFragment newInstance(){
        return new StudentDocumentUploadFragment();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.resumelinearlayour:
                selectImage();
                break;
            case R.id.uploadresume_button:
                UploadResume();
                break;
        }
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setMessage("Uploading Resume...");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
