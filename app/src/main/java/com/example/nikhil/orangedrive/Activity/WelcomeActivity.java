package com.example.nikhil.orangedrive.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.example.nikhil.orangedrive.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

/**
 * Created by Nikhil on 11/26/2017.
 */

public class WelcomeActivity extends AppCompatActivity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        Runnable r = new Runnable() {

            @Override
            public void run() {
                // if you are redirecting from a fragment then use getActivity() as the context.
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if(user == null)
                    startActivity(new Intent(WelcomeActivity.this, MainActivity.class));
                else{
                    String email = user.getEmail();
                    if(email.contains("syr.edu"))
                        startActivity(new Intent(WelcomeActivity.this, OrangeDriveStudentActivity.class));
                    else
                        startActivity(new Intent(WelcomeActivity.this, OrangeDriveCompanyActivity.class));
                }
            }
        };

        Handler h = new Handler();
        // The Runnable will be executed after the given delay time
        h.postDelayed(r, 1000);
    }
}
