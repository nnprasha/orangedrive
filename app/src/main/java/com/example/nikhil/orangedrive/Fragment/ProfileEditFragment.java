package com.example.nikhil.orangedrive.Fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.StringSignature;
import com.example.nikhil.orangedrive.Activity.MainActivity;
import com.example.nikhil.orangedrive.Activity.OrangeDriveStudentActivity;
import com.example.nikhil.orangedrive.Activity.ProfileEditActivity;
import com.example.nikhil.orangedrive.Interface.ProfileEditListener;
import com.example.nikhil.orangedrive.Interface.StudentProfileListener;
import com.example.nikhil.orangedrive.Models.User;
import com.example.nikhil.orangedrive.R;
import com.example.nikhil.orangedrive.Utility.Utility;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Nikhil on 11/28/2017.
 */

public class ProfileEditFragment extends Fragment implements View.OnClickListener {


    ProfileEditListener mListener;
    EditText mName;
    EditText mPhno;
    EditText mBday;
    EditText mCgpa;
    EditText mAddress;
    Button mUpdate;
    ImageView mProfileImage;

    private String profileUrl;
    private DatabaseReference mUsersReference;
    private static final String users_node="Students";
    private ValueEventListener mUserListener;
    public ProgressDialog mProgressDialog;
    FloatingActionButton mCameraIcon;

    private FirebaseStorage storage;
    StorageReference profileImagesStorageRef;
    private static String profileimages_node="profileimages/";

    private String userChoosenTask;
    private static int REQUEST_CAMERA = 1;
    private static int SELECT_FILE = 2;

    public ProfileEditFragment() {}

    public static ProfileEditFragment newInstance(){
        return new ProfileEditFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        //initialize db
        mUsersReference= FirebaseDatabase.getInstance().getReference()
                .child(users_node).child(FirebaseAuth.getInstance().getCurrentUser().getUid());

        storage = FirebaseStorage.getInstance();




        ValueEventListener userDetailsListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);

                mPhno.setText(user.phno);
                mCgpa.setText(user.cgpa);
                mBday.setText(user.bday);
                mName.setText(user.name);
                mAddress.setText(user.address);
                profileUrl = user.profileurl;

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        mUsersReference.addValueEventListener(userDetailsListener);
        mUserListener = userDetailsListener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mRootView = inflater.inflate(R.layout.fragment_profileedit, container, false);

        try{
            mListener=(ProfileEditListener) getContext();
        }catch(ClassCastException e){
            throw new ClassCastException("The hosting activity doesn't implement the interface");
        }

        Toolbar toolbar = mRootView.findViewById(R.id.flexible_example_toolbar);
        toolbar.setNavigationOnClickListener(new Toolbar.OnClickListener(){

            @Override
            public void onClick(View v) {
                mListener.backPressed();
            }

        });

        toolbar.setTitle("Edit Profile");

        //views
        mName = mRootView.findViewById(R.id.name);
        mCgpa = mRootView.findViewById(R.id.cgpa);
        mAddress = mRootView.findViewById(R.id.address);
        mBday = mRootView.findViewById(R.id.bday);
        mPhno = mRootView.findViewById(R.id.phno);
        mUpdate = mRootView.findViewById(R.id.update_button);
//        mProfileImage = mRootView.findViewById(R.id.pImage);
//        mCameraIcon = mRootView.findViewById(R.id.iv_camera);

        mUpdate.setOnClickListener(this);
        //mCameraIcon.setOnClickListener(this);
        return mRootView;
    }

    @Override
    public void onStart() {
        super.onStart();

//        ValueEventListener userDetailsListener = new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                User user = dataSnapshot.getValue(User.class);
//
//                mPhno.setText(user.phno);
//                mCgpa.setText(user.cgpa);
//                mBday.setText(user.bday);
//                mName.setText(user.name);
//                mAddress.setText(user.address);
//                profileUrl = user.profileurl;
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        };
//
//        mUsersReference.addValueEventListener(userDetailsListener);
//        mUserListener = userDetailsListener;
    }

    @Override
    public void onResume() {
        super.onResume();

        ((ProfileEditActivity)getActivity()).getSupportActionBar().hide();
    }

    @Override
    public void onStop() {
        super.onStop();

        ((ProfileEditActivity)getActivity()).getSupportActionBar().show();

        if(mUserListener != null){
            mUsersReference.removeEventListener(mUserListener);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.update_button:
                UpdateUserDetails();
                break;
            case R.id.iv_camera:
                selectImage();
                break;
        }
    }

    public void UpdateUserDetails(){

        showProgressDialog();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                FirebaseUser fbuser = FirebaseAuth.getInstance().getCurrentUser();
//                User user = new User(fbuser.getEmail(),
//                        mName.getText().toString(), mPhno.getText().toString(),
//                        mBday.getText().toString(),mCgpa.getText().toString(),
//                        mAddress.getText().toString(), profileUrl,"");
//
//                Map<String, Object> userValues = user.toMap();
//                Map<String, Object> childUpdates = new HashMap<>();

                DatabaseReference mDatabse = FirebaseDatabase.getInstance().getReference()
                        .child(users_node+"/"+fbuser.getUid()+"/").getRef();
                //childUpdates.put("/" + users_node + "/" + fbuser.getUid(), userValues);

                mDatabse.child("address").setValue(mAddress.getText().toString());
                mDatabse.child("bday").setValue(mBday.getText().toString());
                mDatabse.child("cgpa").setValue(mCgpa.getText().toString());
                mDatabse.child("name").setValue(mName.getText().toString());
                mDatabse.child("phno").setValue(mPhno.getText().toString());
                //mDatabse.updateChildren(childUpdates);
                hideProgressDialog();
                mListener.updateSuccessful();
            }
        }, 500);
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setMessage("Updating…");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    private void selectImage() {
        final CharSequence[] items = { "Take Photo", "Choose from Library",
                "Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result= Utility.checkPermission(getContext());
                if (items[item].equals("Take Photo")) {
                    userChoosenTask="Take Photo";
                    if(result)
                        cameraIntent();
                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask="Choose from Library";
                    if(result)
                        galleryIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void cameraIntent()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void galleryIntent()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if(userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm=null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //mProfileImage.setImageBitmap(bm);
        //ivImage.setImageBitmap(bm);
        UploadNewProfileImageToFirebase(bm);
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap bm = (Bitmap) data.getExtras().get("data");
        UploadNewProfileImageToFirebase(bm);
    }

    public void UploadNewProfileImageToFirebase(final Bitmap bitmap){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();


        UploadTask uploadTask = profileImagesStorageRef.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                //Uri downloadUrl = taskSnapshot.getDownloadUrl();
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                Log.d("Yipppiiiii", downloadUrl.toString());
                mProfileImage.setImageBitmap(bitmap);
            }
        });
    }
}
