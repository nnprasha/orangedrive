package com.example.nikhil.orangedrive.Models;

/**
 * Created by Nikhil on 11/29/2017.
 */


import com.google.firebase.database.Exclude;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Nikhil on 11/6/2017.
 */

//POJO
public class Jobs implements Serializable{


    public String getBasicQualifications() {
        return BasicQualifications;
    }

    public void setBasicQualifications(String basicQualifications) {
        BasicQualifications = basicQualifications;
    }

    public String getCompanyId() {
        return CompanyId;
    }

    public void setCompanyId(String companyId) {
        CompanyId = companyId;
    }

    public String getJobDescription() {
        return JobDescription;
    }

    public void setJobDescription(String jobDescription) {
        JobDescription = jobDescription;
    }

    public String getJobId() {
        return JobId;
    }

    public void setJobId(String jobId) {
        JobId = jobId;
    }

    public String getJobTitle() {
        return JobTitle;
    }

    public void setJobTitle(String jobTitle) {
        JobTitle = jobTitle;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public String getPreferredQualifications() {
        return PreferredQualifications;
    }

    public void setPreferredQualifications(String preferredQualifications) {
        PreferredQualifications = preferredQualifications;
    }

    public String getSalaryRange() {
        return SalaryRange;
    }

    public void setSalaryRange(String salaryRange) {
        SalaryRange = salaryRange;
    }

    public String getSeniorityLevel() {
        return SeniorityLevel;
    }

    public void setSeniorityLevel(String seniorityLevel) {
        SeniorityLevel = seniorityLevel;
    }

    public String getJoblogourl() {
        return joblogourl;
    }

    public void setJoblogourl(String joblogourl) {
        this.joblogourl = joblogourl;
    }


    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }


    private String BasicQualifications;
    private String CompanyId;
    private String JobDescription;
    private String JobId;
    private String JobTitle;
    private String Location;
    private String PreferredQualifications;
    private String SalaryRange;
    private String SeniorityLevel;
    private String CompanyName;
    private String joblogourl;

    public Jobs(){}

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("BasicQualifications", BasicQualifications);
        result.put("CompanyId", CompanyId);
        result.put("JobDescription", JobDescription);
        result.put("JobId", JobId);
        result.put("JobTitle", JobTitle);
        result.put("Location", Location);
        result.put("PreferredQualifications", PreferredQualifications);
        result.put("SalaryRange", SalaryRange);
        result.put("SeniorityLevel", SeniorityLevel);
        result.put("CompanyName", CompanyName);
        result.put("joblogourl", joblogourl);
        return result;
    }
}