package com.example.nikhil.orangedrive.Adapter;

/**
 * Created by Nikhil on 11/29/2017.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.nikhil.orangedrive.Models.Jobs;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.List;
import java.util.Map;

import com.example.nikhil.orangedrive.R;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

/**
 * Created by Nikhil on 11/6/2017.
 */

public class MyFirebaseRecyclerAdapter extends FirebaseRecyclerAdapter<Jobs, MyFirebaseRecyclerAdapter.MovieViewHolder> {

    private Context mContext;
    private List<Map<String, ?>> localMovieData;
    public boolean check=false;
    private static String preUrl="http://image.tmdb.org/t/p/w92/";
    static OnRecyclerViewItemClickListener onClickListener;
    private FirebaseStorage storage;
    StorageReference companyLogosImagesStorageRef;
    public static String companylogoimages_node="companylogos/";

    /**
     * @param modelClass      Firebase will marshall the data at a location into
     *                        an instance of a class that you provide
     * @param modelLayout     This is the layout used to represent a single item in the list.
     *                        You will be responsible for populating an instance of the corresponding
     *                        view with the data from an instance of modelClass.
     * @param viewHolderClass The class that hold references to all sub-views in an instance modelLayout.
     * @param ref             The Firebase location to watch for data changes. Can also be a slice of a location,
     *                        using some combination of {@code limit()}, {@code startAt()}, and {@code endAt()}.
     */
    public MyFirebaseRecyclerAdapter(Class<Jobs> modelClass, int modelLayout, Class<MovieViewHolder> viewHolderClass, Query ref, Context context) {
        super(modelClass, modelLayout, viewHolderClass, ref);
        this.mContext=context;
        storage = FirebaseStorage.getInstance();
    }

    public void setOnItemClickListener(final OnRecyclerViewItemClickListener mItemClickListener){
        this.onClickListener=mItemClickListener;
    }

    @Override
    protected void populateViewHolder(final MovieViewHolder viewHolder, final Jobs model, int position) {

        viewHolder.cardDesc.setText((String) model.getCompanyName() + " | " + model.getLocation());
        viewHolder.cardTitle.setText((String) model.getJobTitle());

        companyLogosImagesStorageRef = storage.getReference().child(companylogoimages_node+model.getJoblogourl());

        Glide.with(mContext)
                .using(new FirebaseImageLoader())
                .load(companyLogosImagesStorageRef)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(viewHolder.cardImage);

        viewHolder.cardImage.setTransitionName(model.getCompanyId()+"_"+model.getJobId());

        DatabaseReference studentApplnRef = FirebaseDatabase.getInstance().getReference().child("Students")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid().toString())
                .child("jobapplications").getRef();

        studentApplnRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild(model.getCompanyId()+"_"+model.getJobId())) {
                    Log.d("yoyooy","yessssssssssssssssssssss************");
                    // run some code
                    viewHolder.cardTick.setVisibility(View.VISIBLE);
                    viewHolder.cardLinearLayout.setBackgroundResource(R.color.colorAccent);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }


        });
        //Picasso.with(mContext).load((String) model.getJoblogourl()).into(viewHolder.cardImage);
    }

    public static class MovieViewHolder extends RecyclerView.ViewHolder{

        public TextView cardTitle;
        public TextView cardDesc;
        public ImageView cardImage;
        public ImageView cardTick;
        public LinearLayout cardLinearLayout;

        public MovieViewHolder(View v) {
            super(v);
            cardTitle=(TextView) v.findViewById(R.id.cardtitle);
            cardDesc=(TextView) v.findViewById(R.id.carddescription);
            cardImage=(ImageView) v.findViewById(R.id.cardImage);
            cardTick=(ImageView) v.findViewById(R.id.cardTick);
            cardLinearLayout=(LinearLayout) v.findViewById(R.id.cardLinearLayout);

            v.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    if(onClickListener != null){
                        if(getAdapterPosition() != RecyclerView.NO_POSITION){
                            // Toast.makeText(v.getContext(), "Toast Message",Toast.LENGTH_SHORT).show();
                            // to make a click event generic, we are not performing any action here. we are just calling
                            // another function..
                            onClickListener.OnItemClick(v, getAdapterPosition());
                        }
                        //movieItemClickListener.onItemClick(v, getPosition());
                    }
                }
            });
        }
    }

    public interface OnRecyclerViewItemClickListener{
        public void OnItemClick(final View v, int position);
    }
}

