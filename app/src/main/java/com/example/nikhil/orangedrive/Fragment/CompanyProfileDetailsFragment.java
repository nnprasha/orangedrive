package com.example.nikhil.orangedrive.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.nikhil.orangedrive.Interface.CompanyProfileListener;
import com.example.nikhil.orangedrive.Interface.StudentProfileListener;
import com.example.nikhil.orangedrive.Models.Company;
import com.example.nikhil.orangedrive.Models.User;
import com.example.nikhil.orangedrive.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * Created by Nikhil on 12/1/2017.
 */

public class CompanyProfileDetailsFragment extends Fragment {
    TextView mCompanyName;
    TextView mCompanyId;
    TextView mRecruiterName;
    TextView mRecruiterEmail;

    private DatabaseReference mCompanyReference;
    private static final String company_node="Companies";
    private ValueEventListener mCompanyListener;
    CompanyProfileListener mListener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        //initialize db
        mCompanyReference= FirebaseDatabase.getInstance().getReference()
                .child(company_node).child(FirebaseAuth.getInstance().getCurrentUser().getUid());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mRootView = inflater.inflate(R.layout.fragment_companyprofiledetails, container, false);

        try{
            mListener=(CompanyProfileListener) getContext();
        }catch(ClassCastException e){
            throw new ClassCastException("The hosting activity doesn't implement the interface");
        }

        //views
        mCompanyName = mRootView.findViewById(R.id.companyname_value);
        mRecruiterName = mRootView.findViewById(R.id.recruitername_value);
        mRecruiterEmail = mRootView.findViewById(R.id.recruiteremail_value);
        mCompanyId = mRootView.findViewById(R.id.companyid_value);

        return mRootView;
    }

    @Override
    public void onStart() {
        super.onStart();

        ValueEventListener userDetailsListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Company company = dataSnapshot.getValue(Company.class);

                mCompanyId.setText(company.getCompanyid());
                mCompanyName.setText(company.getCompanyname());
                mRecruiterEmail.setText(company.getEmail());
                mRecruiterName.setText(company.getRecruitername());

                mListener.changeProfileNameAndImage(company.getCompanyname(),
                        company.getLogourl());
                //mPassword.setText(FirebaseAuth.getInstance().getCurrentUser());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        mCompanyReference.addValueEventListener(userDetailsListener);
        mCompanyListener = userDetailsListener;
    }

    @Override
    public void onStop() {
        super.onStop();

        if(mCompanyListener != null){
            mCompanyReference.removeEventListener(mCompanyListener);
        }
    }

    public static Fragment newInstance() {
        return new CompanyProfileDetailsFragment();
    }
}
