package com.example.nikhil.orangedrive.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.nikhil.orangedrive.Models.Applicants;
import com.example.nikhil.orangedrive.Models.Jobs;
import com.example.nikhil.orangedrive.Models.User;
import com.example.nikhil.orangedrive.R;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.database.Query;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.w3c.dom.Text;

import java.util.List;
import java.util.Map;

/**
 * Created by Nikhil on 12/5/2017.
 */

public class MyApplicantsRecyclerAdapter extends FirebaseRecyclerAdapter<Applicants, MyApplicantsRecyclerAdapter.MovieViewHolder> {

    private Context mContext;
    private List<Map<String, ?>> localMovieData;
    public boolean check=false;
    private static String preUrl="http://image.tmdb.org/t/p/w92/";
    static MyApplicantsRecyclerAdapter.OnRecyclerViewItemClickListener onClickListener;
    private FirebaseStorage storage;
    StorageReference companyLogosImagesStorageRef;
    public static String profilepictures="profileimages/";
    /**
     * @param modelClass      Firebase will marshall the data at a location into
     *                        an instance of a class that you provide
     * @param modelLayout     This is the layout used to represent a single item in the list.
     *                        You will be responsible for populating an instance of the corresponding
     *                        view with the data from an instance of modelClass.
     * @param viewHolderClass The class that hold references to all sub-views in an instance modelLayout.
     * @param ref             The Firebase location to watch for data changes. Can also be a slice of a location,
     *                        using some combination of {@code limit()}, {@code startAt()}, and {@code endAt()}.
     */
    public MyApplicantsRecyclerAdapter(Class<Applicants> modelClass, int modelLayout, Class<MyApplicantsRecyclerAdapter.MovieViewHolder> viewHolderClass, Query ref, Context context) {
        super(modelClass, modelLayout, viewHolderClass, ref);
        this.mContext=context;
        storage = FirebaseStorage.getInstance();
    }

    public void setOnItemClickListener(final MyApplicantsRecyclerAdapter.OnRecyclerViewItemClickListener mItemClickListener){
        this.onClickListener=mItemClickListener;
    }


    @Override
    protected void populateViewHolder(MyApplicantsRecyclerAdapter.MovieViewHolder viewHolder, Applicants model, int position) {

        viewHolder.cardTitle.setText(model.name);
        viewHolder.cardNetId.setText(model.netid);
        viewHolder.cardCgpa.setText(model.cgpa);
        viewHolder.cardJobId.setText(model.appliedfor);

        companyLogosImagesStorageRef = storage.getReference().child(profilepictures+model.profileurl);

        Glide.with(mContext)
                .using(new FirebaseImageLoader())
                .load(companyLogosImagesStorageRef)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(viewHolder.cardImage);

        viewHolder.cardImage.setTransitionName(model.id);
    }

    public static class MovieViewHolder extends RecyclerView.ViewHolder{

        public TextView cardTitle;
        public TextView cardNetId;
        public TextView cardCgpa;
        public ImageView cardImage;
        public TextView cardJobId;
        public MovieViewHolder(View v) {
            super(v);
            cardTitle= v.findViewById(R.id.cardtitle);
            cardNetId= v.findViewById(R.id.cardnetiid);
            cardCgpa = v.findViewById(R.id.cardcgpa);
            cardImage= v.findViewById(R.id.student_profileimage);
            cardJobId = v.findViewById(R.id.cardJobId);

            v.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    if(onClickListener != null){
                        if(getAdapterPosition() != RecyclerView.NO_POSITION){
                            // Toast.makeText(v.getContext(), "Toast Message",Toast.LENGTH_SHORT).show();
                            // to make a click event generic, we are not performing any action here. we are just calling
                            // another function..
                            onClickListener.OnItemClick(v, getAdapterPosition());
                        }
                    }
                }
            });
        }
    }

    public interface OnRecyclerViewItemClickListener{
        void OnItemClick(final View v, int position);
    }
}
