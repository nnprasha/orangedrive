package com.example.nikhil.orangedrive.Fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.disklrucache.DiskLruCache;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.nikhil.orangedrive.Activity.OrangeDriveCompanyActivity;
import com.example.nikhil.orangedrive.Activity.OrangeDriveStudentActivity;
import com.example.nikhil.orangedrive.Interface.JobDetailViewListener;
import com.example.nikhil.orangedrive.Interface.StudentLoginFragmentListener;
import com.example.nikhil.orangedrive.Models.Applicants;
import com.example.nikhil.orangedrive.Models.Jobs;
import com.example.nikhil.orangedrive.Models.User;
import com.example.nikhil.orangedrive.R;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Nikhil on 11/29/2017.
 */

public class JobDetailViewFragment extends Fragment implements AppBarLayout.OnOffsetChangedListener,
View.OnClickListener{
    private static final String ARG_MOVIE="MOVIE";
    private static final int PERCENTAGE_TO_SHOW_IMAGE = 20;
    private View mFab;
    private int mMaxScrollSize;
    private boolean mIsImageHidden;
    HashMap<String,?> movie;

    JobDetailViewListener mListener;
    TextView mBasicQ;
    TextView mPreferredQ;
    TextView mJobTitle;
    TextView mJobDescription;
    TextView mSalaryRange;
    TextView mJobId;
    TextView mSeniorityLevel;
    TextView mJobLocation;

    ImageView mJobImg;

    String companyId;
    String companyName;
    String location;
    String joblogourl;

    String student_node = "Students";
    public ProgressDialog mProgressDialog;
    boolean isCompany;
    Query query1;
    Query query2;

    private FirebaseStorage storage;
    StorageReference companyLogosImagesStorageRef;
    public static String companylogoimages_node="companylogos/";

    Button jobWithdraw;

    private DatabaseReference mUsersReference;
    private static final String users_node="Students";
    private ValueEventListener mUserListener;
    private ValueEventListener mWithdrawJobListener;
    private ValueEventListener mApplyForJobListener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        storage = FirebaseStorage.getInstance();
        mUsersReference= FirebaseDatabase.getInstance().getReference()
                .child(users_node).child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        if(getArguments()!=null)
        {
            movie=(HashMap<String,?>) getArguments().getSerializable(ARG_MOVIE);
            isCompany= getArguments().getBoolean("IsCompany");
        }
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
        if (mMaxScrollSize == 0)
            mMaxScrollSize = appBarLayout.getTotalScrollRange();

        int currentScrollPercentage = (Math.abs(i)) * 100
                / mMaxScrollSize;

        if (currentScrollPercentage >= PERCENTAGE_TO_SHOW_IMAGE) {
            if (!mIsImageHidden) {
                mIsImageHidden = true;

                ViewCompat.animate(mFab).scaleY(0).scaleX(0).start();
            }
        }

        if (currentScrollPercentage < PERCENTAGE_TO_SHOW_IMAGE) {
            if (mIsImageHidden) {
                mIsImageHidden = false;
                ViewCompat.animate(mFab).scaleY(1).scaleX(1).start();
            }
        }
    }

    public static JobDetailViewFragment newInstance(HashMap<String, ?> movie, boolean isCompany){
        JobDetailViewFragment fragment = new JobDetailViewFragment();
        Bundle args=new Bundle();
        args.putSerializable(ARG_MOVIE,movie);
        args.putBoolean("IsCompany", isCompany);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mRootView = inflater.inflate(R.layout.fragment_jobdetailview,container,false);

        mFab = mRootView.findViewById(R.id.flexible_example_fab);
        mFab.setOnClickListener(this);

        jobWithdraw = mRootView.findViewById(R.id.jobwithdraw_button);
        jobWithdraw.setOnClickListener(this);

        companyId = movie.get("CompanyId").toString();
        companyName = movie.get("CompanyName").toString();
        location = movie.get("Location").toString();
        joblogourl = movie.get("joblogourl").toString();

        if(!isCompany){
            DatabaseReference studentApplnRef = FirebaseDatabase.getInstance().getReference().child("Students")
                    .child(FirebaseAuth.getInstance().getCurrentUser().getUid().toString())
                    .child("jobapplications");

            studentApplnRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String child = companyId+"_"+movie.get("JobId").toString();
                    if (dataSnapshot.hasChild(child)) {
                        Log.d("yoyooy","yessssssssssssssssssssss************");
                        // run some code
                        jobWithdraw.setVisibility(View.VISIBLE);
                    }else{
                        mFab.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }


        AppBarLayout appbar = mRootView.findViewById(R.id.flexible_example_appbar);
        appbar.addOnOffsetChangedListener(this);

        try{
            mListener=(JobDetailViewListener) getContext();
        }catch(ClassCastException e){
            throw new ClassCastException("The hosting activity doesn't implement the interface");
        }

        Toolbar toolbar = mRootView.findViewById(R.id.flexible_example_toolbar);
        toolbar.setNavigationOnClickListener(new Toolbar.OnClickListener(){

            @Override
            public void onClick(View v) {
                mListener.backPressed();
            }

        });

        toolbar.setTitle(movie.get("CompanyName").toString());


        //views
        mBasicQ=mRootView.findViewById(R.id.basicqualifications);
        mJobDescription=mRootView.findViewById(R.id.jobdescription);
        mPreferredQ=mRootView.findViewById(R.id.preferredqualifications);
        mJobId=mRootView.findViewById(R.id.jobid);
        mSalaryRange=mRootView.findViewById(R.id.salaryrange);
        mSeniorityLevel=mRootView.findViewById(R.id.senioritylevel);
        mJobTitle=mRootView.findViewById(R.id.jobtitle);
        mJobImg=mRootView.findViewById(R.id.jobimage);
        mJobLocation = mRootView.findViewById(R.id.joblocation);

        //Picasso.with(getContext()).load((String)movie.get("joblogourl")).into(mJobImg);
        companyLogosImagesStorageRef = storage.getReference().child(companylogoimages_node+movie.get("joblogourl"));

        Glide.with(getContext())
                .using(new FirebaseImageLoader())
                .load(companyLogosImagesStorageRef)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(mJobImg);


        mJobImg.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorTint), android.graphics.PorterDuff.Mode.MULTIPLY);
        //mJobImg.setImageTintMode();


        mBasicQ.setText(movie.get("BasicQualifications").toString());
        //mBasicQ.setText("\u2022 Bullet");
        mJobDescription.setText(movie.get("JobDescription").toString());
        mJobId.setText(movie.get("JobId").toString());
        mPreferredQ.setText(movie.get("PreferredQualifications").toString());
        mSeniorityLevel.setText(movie.get("SeniorityLevel").toString());
        mSalaryRange.setText(movie.get("SalaryRange").toString());
        mJobTitle.setText(movie.get("JobTitle").toString());
        mJobLocation.setText(movie.get("Location").toString());

        String transitionName = movie.get("CompanyId").toString() +"_"+ movie.get("JobId").toString();
        mJobImg.setTransitionName(transitionName);
        return mRootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!isCompany)
            ((OrangeDriveStudentActivity)getActivity()).getSupportActionBar().hide();
        else
            ((OrangeDriveCompanyActivity)getActivity()).getSupportActionBar().hide();
    }
    @Override
    public void onStop() {
        super.onStop();
        if(!isCompany)
            ((OrangeDriveStudentActivity)getActivity()).getSupportActionBar().show();
        else
            ((OrangeDriveCompanyActivity)getActivity()).getSupportActionBar().show();

        if(mUserListener != null){
            mUsersReference.removeEventListener(mUserListener);
        }

        if(mWithdrawJobListener!=null){
            query1.removeEventListener(mWithdrawJobListener);
        }

        if(mApplyForJobListener!=null){
            query2.removeEventListener(mApplyForJobListener);
        }



    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.flexible_example_fab:
                ApplyForJob();
                break;
            case R.id.jobwithdraw_button:
                WithdrawJob();
                break;
        }
    }

    public void WithdrawJob(){
        showProgressDialog("Withdrawing application ...");
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                DatabaseReference studentApplnRef = FirebaseDatabase.getInstance().getReference().child("Students")
                        .child(FirebaseAuth.getInstance().getCurrentUser().getUid().toString())
                        .child("jobapplications");

                studentApplnRef.child(companyId+"_"+movie.get("JobId").toString()).removeValue();

                query1 = FirebaseDatabase.getInstance().getReference().child("Companies")
                        .orderByChild("companyid").equalTo(companyId);

                ValueEventListener withdrawJob = new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        HashMap<String, ?> map = (HashMap<String, ?>)dataSnapshot.getValue();

                        String key=null;
                        for(String k: map.keySet()){
                            key = k;
                        }

                        DatabaseReference companyRef = FirebaseDatabase.getInstance().getReference().child("Companies")
                                .child(key);

                        companyRef.child("myapplicants").child(FirebaseAuth.getInstance().getCurrentUser().getUid().toString()+"_"+movie.get("JobId").toString())
                                .removeValue();


                        hideProgressDialog();
                        mListener.backPressed();
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                };

                query1.addValueEventListener(withdrawJob);
                mWithdrawJobListener = withdrawJob;

            }
        }, 500);


    }

    public void ApplyForJob(){

        showProgressDialog("Applying for job ... ");
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Jobs job = new Jobs();
                job.setCompanyId(companyId);
                job.setCompanyName(companyName);
                job.setBasicQualifications(mBasicQ.getText().toString());
                job.setJobDescription(mJobDescription.getText().toString());
                job.setJobId(mJobId.getText().toString());
                job.setJobTitle(mJobTitle.getText().toString());
                job.setPreferredQualifications(mPreferredQ.getText().toString());
                job.setSalaryRange(mSalaryRange.getText().toString());
                job.setSeniorityLevel(mSeniorityLevel.getText().toString());
                job.setLocation(location);
                job.setJoblogourl(joblogourl);

                Map<String, Object> jobValues = job.toMap();
                Map<String, Object> childUpdates = new HashMap<>();
                DatabaseReference mDatabse = FirebaseDatabase.getInstance().getReference();

                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                childUpdates.put(student_node+ "/" + user.getUid()+ "/" + "jobapplications" + "/" +
                        job.getCompanyId()+"_"+job.getJobId(), jobValues);

                mDatabse.updateChildren(childUpdates);

                DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                        .child("Companies").getRef();

                query2 = ref.orderByChild("companyid").equalTo(companyId);

                ValueEventListener applyForJobListener = new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        //if (dataSnapshot.exists()) {
                        // dataSnapshot is the "issue" node with all children with id 0
                        HashMap<String, ?> map = (HashMap<String, ?>)dataSnapshot.getValue();
                        String companyId=null;
                        for(String k: map.keySet())
                        {
                            companyId=k;
                        }
                        Log.d("yo",companyId);

                        AddMyApplicants(companyId, mJobId.getText().toString());
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                };
                query2.addValueEventListener(applyForJobListener);
                mApplyForJobListener=applyForJobListener;

            }
        }, 500);
    }
    public void AddMyApplicants(final String companyId, final String jobId){
        ValueEventListener userDetailsListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);



                Applicants newApplicant = new Applicants(user.netid,user.name,user.phno,user.bday,user.cgpa,user.address
                ,user.profileurl,user.resumeurl,jobId,FirebaseAuth.getInstance().getCurrentUser().getUid()+"_"+jobId);


                Map<String, Object> applicantValues = newApplicant.toMap();
                Map<String, Object> childUpdates = new HashMap<>();

                DatabaseReference mDatabse = FirebaseDatabase.getInstance().getReference();

                childUpdates.put("Companies/"+companyId+"/"+"myapplicants/"+
                        FirebaseAuth.getInstance().getCurrentUser().getUid()+"_"+jobId, applicantValues);

                mDatabse.updateChildren(childUpdates);



                hideProgressDialog();
                mListener.backPressed();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        mUsersReference.addValueEventListener(userDetailsListener);
        mUserListener = userDetailsListener;
    }


    public void showProgressDialog(String text) {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setMessage(text);
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
