package com.example.nikhil.orangedrive.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.example.nikhil.orangedrive.Fragment.CompanyLoginFragment;
import com.example.nikhil.orangedrive.Fragment.RegisterCompanyFragment;
import com.example.nikhil.orangedrive.Fragment.RegisterUserFragment;
import com.example.nikhil.orangedrive.Fragment.StudentLoginFragment;
import com.example.nikhil.orangedrive.Interface.CompanyLoginFragmentListener;
import com.example.nikhil.orangedrive.R;
import com.google.firebase.auth.FirebaseAuth;

/**
 * Created by Nikhil on 11/30/2017.
 */

public class CompanyLoginActivity extends AppCompatActivity implements CompanyLoginFragmentListener {


    Toolbar toolbar;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    public ProgressDialog mProgressDialog;

    private static final String TAG = "CompanyLoginActivity";

    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_companylogin);

        setUpToolBar();

        if(savedInstanceState == null)
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.companyloginpagecontainer, CompanyLoginFragment.newInstance())
                    .commit();
    }

    private void setUpToolBar(){
        toolbar=(Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setTitleTextColor(getColor(R.color.colorWhite));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setToolBarTitle(String name) {
        this.toolbar.setTitle(name);
        this.toolbar.setTitleTextColor(getColor(R.color.colorWhite));
    }

    @Override
    public void onLoadRegisterFragment() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.companyloginpagecontainer, RegisterCompanyFragment.newInstance())
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onLoadODCActivity() {
        Intent intent = new Intent(CompanyLoginActivity.this, OrangeDriveCompanyActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.from_middle, R.anim.to_middle);
    }

    @Override
    public void backPressed() {
        onBackPressed();
    }
}
