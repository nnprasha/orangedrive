package com.example.nikhil.orangedrive.Models;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Nikhil on 12/5/2017.
 */

public class Applicants {
    public String name;
    public String netid;
    public String phno;
    public String bday;
    public String address;
    public String cgpa;
    public String profileurl;
    public String resumeurl;
    public String appliedfor;
    public String id;
    public Applicants() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)

    }

    public Applicants(String netid,
                String name,
                String phno,
                String bday,
                String cgpa,
                String address,
                String profileurl,
                String resumeurl, String appliedfor, String id) {
        this.netid = netid;
        this.name = name;
        this.phno = phno;
        this.bday = bday;
        this.address = address;
        this.cgpa = cgpa;
        this.profileurl = profileurl;
        this.resumeurl = resumeurl;
        this.appliedfor = appliedfor;
        this.id = id;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("netid", netid);
        result.put("name", name);
        result.put("phno", phno);
        result.put("bday", bday);
        result.put("address", address);
        result.put("cgpa", cgpa);
        result.put("profileurl", profileurl);
        result.put("resumeurl", resumeurl);
        result.put("appliedfor",appliedfor);
        result.put("id",id);
        return result;
    }

}
