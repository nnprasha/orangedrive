package com.example.nikhil.orangedrive.Activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.RelativeLayout;

import com.example.nikhil.orangedrive.Fragment.ProfileEditFragment;
import com.example.nikhil.orangedrive.Fragment.StudentLoginFragment;
import com.example.nikhil.orangedrive.Interface.ProfileEditListener;
import com.example.nikhil.orangedrive.R;
import com.example.nikhil.orangedrive.Utility.Utility;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by Nikhil on 11/28/2017.
 */

public class ProfileEditActivity extends AppCompatActivity implements ProfileEditListener{

    Toolbar toolbar;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profileedit);

        setUpToolBar();

        if(savedInstanceState == null)
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.editprofilecontainer, ProfileEditFragment.newInstance())
                    .commit();
    }

    private void setUpToolBar(){
        toolbar=(Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setTitleTextColor(getColor(R.color.colorWhite));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setToolBarTitle(String name) {
        toolbar.setTitle(name);
        toolbar.setTitleTextColor(getColor(R.color.colorWhite));
    }

    @Override
    public void backPressed() {
        onBackPressed();
    }

    @Override
    public void updateSuccessful() {
        onBackPressed();
    }




}
