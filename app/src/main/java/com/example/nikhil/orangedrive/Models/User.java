package com.example.nikhil.orangedrive.Models;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Nikhil on 11/27/2017.
 */

public class User {
    public String name;
    public String netid;
    public String phno;
    public String bday;
    public String address;
    public String cgpa;
    public String profileurl;
    public String resumeurl;

    public User() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)

    }

    public User(String netid,
                String name,
                String phno,
                String bday,
                String cgpa,
                String address,
                String profileurl,
                String resumeurl) {
        this.netid = netid;
        this.name = name;
        this.phno = phno;
        this.bday = bday;
        this.address = address;
        this.cgpa = cgpa;
        this.profileurl = profileurl;
        this.resumeurl = resumeurl;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("netid", netid);
        result.put("name", name);
        result.put("phno", phno);
        result.put("bday", bday);
        result.put("address", address);
        result.put("cgpa", cgpa);
        result.put("profileurl", profileurl);
        return result;
    }
}
