package com.example.nikhil.orangedrive.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.nikhil.orangedrive.Adapter.MyFirebaseRecyclerAdapter;
import com.example.nikhil.orangedrive.Adapter.MyJobsRecyclerAdapter;
import com.example.nikhil.orangedrive.Data.MyJobsData;
import com.example.nikhil.orangedrive.Interface.CompanyJobsRecyclerViewListener;
import com.example.nikhil.orangedrive.Interface.CompanyLoginFragmentListener;
import com.example.nikhil.orangedrive.Models.Jobs;
import com.example.nikhil.orangedrive.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.FlipInBottomXAnimator;

/**
 * Created by Nikhil on 12/1/2017.
 */

public class CompanyJobPostingRecyclerViewFragment extends Fragment {

    private static String company_node="Companies";

    RecyclerView mRecyclerView;
    RecyclerView.LayoutManager mLayoutManager;
    MyJobsRecyclerAdapter myFirebaseRecyclerAdapter;

    MyJobsData movieData;
    CompanyJobsRecyclerViewListener mListener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View mRootView = inflater.inflate(R.layout.fragment_myjobsrecyclerview, container, false);

        try{
            mListener=(CompanyJobsRecyclerViewListener) getContext();
        }catch(ClassCastException e){
            throw new ClassCastException("The hosting activity doesn't implement the interface");
        }

        mListener.setToolBarTitle("My Job Posting");

        FirebaseUser cuser= FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference childRef= FirebaseDatabase.getInstance().getReference()
                .child(company_node).child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child("myjobs").getRef();

        movieData=new MyJobsData(childRef);


        mRecyclerView = mRootView.findViewById(R.id.myjobscardList);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager=new LinearLayoutManager(getActivity());

        mRecyclerView.setLayoutManager(mLayoutManager);

        //Query query=childRef.orderByKey().equalTo("Company101_Job100");

        myFirebaseRecyclerAdapter=new MyJobsRecyclerAdapter(Jobs.class, R.layout.layout_cardview,
                MyJobsRecyclerAdapter.MovieViewHolder.class, childRef, getContext());

        //Set adapter animation
        //mRecyclerView.setAdapter(new ScaleInAnimationAdapter(myFirebaseRecyclerAdapter));


        ScaleInAnimationAdapter alphaAdapter = new ScaleInAnimationAdapter(myFirebaseRecyclerAdapter);
        alphaAdapter.setDuration(500);
        mRecyclerView.setAdapter(alphaAdapter);



        //mRecyclerView.setAdapter(new AlphaInAnimationAdapter(myFirebaseRecyclerAdapter));
        if (movieData.getSize() == 0) {
            movieData.setAdapter(myFirebaseRecyclerAdapter);
            //getApplicationContext()-activity is used movieData.initializeDataFromCloud();
            movieData.setContext(getActivity());
            movieData.initializeDataFromCloud();
        }
        //defaultAnimation();

        //setting item animation
        itemAnimation();

        myFirebaseRecyclerAdapter.setOnItemClickListener(new MyJobsRecyclerAdapter.OnRecyclerViewItemClickListener(){

            @Override
            public void OnItemClick(final View v, int position) {
                HashMap<String, ?> movie = (HashMap<String, ?>) movieData.getItem(position);
                String id = (String) movie.get("CompanyId") + "_" + movie.get("JobId");
                DatabaseReference ref = movieData.getFireVaseRef();
                ref.child(id).addListenerForSingleValueEvent(new com.google.firebase.database.ValueEventListener(){

                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        HashMap<String, String> movie =  (HashMap<String, String>) dataSnapshot.getValue();
                        mListener.loadMovieDetailFragment(movie,v.findViewById(R.id.cardImage));
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.d("My Test", "The read failed: " + databaseError.getMessage());
                    }
                });
            }
        });

        return mRootView;
    }

    public void itemAnimation()
    {
        FlipInBottomXAnimator animator = new FlipInBottomXAnimator();
        animator.setAddDuration(500);
        mRecyclerView.setItemAnimator(animator);
//        FadeInLeftAnimator animator=new FadeInLeftAnimator();
//        animator.setAddDuration(100);
//        animator.setRemoveDuration(100);
//        mRecyclerView.setItemAnimator(animator);

    }

    public static CompanyJobPostingRecyclerViewFragment newInstance(){
        return new CompanyJobPostingRecyclerViewFragment();
    }
}
