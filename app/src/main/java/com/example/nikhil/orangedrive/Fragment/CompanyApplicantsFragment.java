package com.example.nikhil.orangedrive.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.nikhil.orangedrive.Adapter.MyApplicantsRecyclerAdapter;
import com.example.nikhil.orangedrive.Adapter.MyFirebaseRecyclerAdapter;
import com.example.nikhil.orangedrive.Data.JobsData;
import com.example.nikhil.orangedrive.Data.MyApplicantsData;
import com.example.nikhil.orangedrive.Interface.CompanyApplicantsListener;
import com.example.nikhil.orangedrive.Interface.CompanyJobsRecyclerViewListener;
import com.example.nikhil.orangedrive.Models.Applicants;
import com.example.nikhil.orangedrive.Models.Jobs;
import com.example.nikhil.orangedrive.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.FlipInBottomXAnimator;

/**
 * Created by Nikhil on 12/4/2017.
 */

public class CompanyApplicantsFragment extends Fragment {

    CompanyApplicantsListener mListener;
    public MyApplicantsData movieData;

    RecyclerView mRecyclerView;
    RecyclerView.LayoutManager mLayoutManager;
    ImageView signout;
    //MyRecyclerViewAdapter mRecyclerViewAdapter;

    MyApplicantsRecyclerAdapter myFirebaseRecyclerAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        final View mRootView = inflater.inflate(R.layout.fragment_companyapplicants,container,false);

        try{
            mListener=(CompanyApplicantsListener) getContext();
        }catch(ClassCastException e){
            throw new ClassCastException("The hosting activity doesn't implement the interface");
        }
        mListener.setToolbarTitle("My Applicants");


        DatabaseReference childRef= FirebaseDatabase.getInstance().getReference()
                .child("Companies")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child("myapplicants")
                .getRef();

        movieData=new MyApplicantsData(childRef);


        mRecyclerView = mRootView.findViewById(R.id.myapplicantscardlist);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager=new LinearLayoutManager(getActivity());

        mRecyclerView.setLayoutManager(mLayoutManager);

        //DatabaseReference childRef= FirebaseDatabase.getInstance().getReference().child("Jobs").getRef();
        myFirebaseRecyclerAdapter=new MyApplicantsRecyclerAdapter(Applicants.class, R.layout.layout_myapplicantscardview,
                MyApplicantsRecyclerAdapter.MovieViewHolder.class, childRef, getContext());

        //Set adapter animation
        mRecyclerView.setAdapter(new ScaleInAnimationAdapter(myFirebaseRecyclerAdapter));
        //mRecyclerView.setAdapter(new AlphaInAnimationAdapter(myFirebaseRecyclerAdapter));
        if (movieData.getSize() == 0) {
            movieData.setAdapter(myFirebaseRecyclerAdapter);
            //getApplicationContext()-activity is used movieData.initializeDataFromCloud();
            movieData.setContext(getActivity());
            movieData.initializeDataFromCloud();
        }
        //defaultAnimation();

        //setting item animation
        itemAnimation();

        myFirebaseRecyclerAdapter.setOnItemClickListener(new MyApplicantsRecyclerAdapter.OnRecyclerViewItemClickListener(){

            @Override
            public void OnItemClick(final View v, int position) {
                HashMap<String, ?> movie = (HashMap<String, ?>) movieData.getItem(position);
                String id = (String) movie.get("id");
                DatabaseReference ref = movieData.getFireVaseRef();
                ref.child(id).addListenerForSingleValueEvent(new com.google.firebase.database.ValueEventListener(){

                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        HashMap<String, String> movie =  (HashMap<String, String>) dataSnapshot.getValue();
                        mListener.onLoadApplicantsDetailView(movie, v.findViewById(R.id.student_profileimage));
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.d("My Test", "The read failed: " + databaseError.getMessage());
                    }
                });
            }
        });

        return mRootView;
    }

    public void itemAnimation()
    {
        FlipInBottomXAnimator animator = new FlipInBottomXAnimator();
        animator.setAddDuration(500);
        mRecyclerView.setItemAnimator(animator);
//        FadeInLeftAnimator animator=new FadeInLeftAnimator();
//        animator.setAddDuration(100);
//        animator.setRemoveDuration(100);
//        mRecyclerView.setItemAnimator(animator);

    }

    public static CompanyApplicantsFragment newInstance(){
        return new CompanyApplicantsFragment();
    }
}
