package com.example.nikhil.orangedrive.Fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.nikhil.orangedrive.Activity.CompanyLoginActivity;
import com.example.nikhil.orangedrive.Activity.StudentLoginActivity;
import com.example.nikhil.orangedrive.Interface.CompanyLoginFragmentListener;
import com.example.nikhil.orangedrive.Interface.StudentLoginFragmentListener;
import com.example.nikhil.orangedrive.Models.Company;
import com.example.nikhil.orangedrive.Models.User;
import com.example.nikhil.orangedrive.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Nikhil on 11/30/2017.
 */

public class RegisterCompanyFragment extends Fragment implements View.OnClickListener {

    private CompanyLoginFragmentListener mListener;
    private EditText mCompanyName;
    private EditText mCompanyId;
    private EditText mPassword;
    private EditText mRecruiterEmail;
    private EditText mRecruiterName;
    private Button mRegister;
    public ProgressDialog mProgressDialog;
    public static String TAG="RegisterCompanyFragment";

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference mDatabase;
    private FirebaseStorage storage;
    private StorageReference companyLogoStorageRef;
    private static String companylogos_node="companylogos";
    private static String users_company_node="Companies";

    public RegisterCompanyFragment(){}

    public static RegisterCompanyFragment newInstance(){
        return new RegisterCompanyFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);


        mAuth=FirebaseAuth.getInstance();
        storage = FirebaseStorage.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        mAuthListener=new FirebaseAuth.AuthStateListener(){

            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if(user!=null){
                    Log.d(TAG, "onAuthStateChanged:signed_in: "+user.getUid());
                } else {
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
            }
        };
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View rootView=inflater.inflate(R.layout.fragment_companyregister,container,false);

        //views
        mCompanyName = rootView.findViewById(R.id.register_cname);
        mCompanyId = rootView.findViewById(R.id.register_cid);
        mPassword = rootView.findViewById(R.id.register_password);
        mRecruiterEmail = rootView.findViewById(R.id.register_recruiteremail);
        mRecruiterName = rootView.findViewById(R.id.register_recruitername);

        //button
        mRegister = rootView.findViewById(R.id.register_button);
        mRegister.setOnClickListener(this);

        try{
            mListener=(CompanyLoginFragmentListener) getContext();
        }catch(ClassCastException e){
            throw new ClassCastException("The hosting activity doesn't implement the interface");
        }

        //mListener.setToolBarTitle("Register");

        Toolbar toolbar = rootView.findViewById(R.id.flexible_example_toolbar);
        toolbar.setNavigationOnClickListener(new Toolbar.OnClickListener(){

            @Override
            public void onClick(View v) {
                mListener.backPressed();
            }

        });

        toolbar.setTitle("Company Register");
        return rootView;
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setMessage("Registering…");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    private boolean validateForm() {
        boolean valid = true;

        String email = mRecruiterEmail.getText().toString();
        if (TextUtils.isEmpty(email)) {
            mRecruiterEmail.setError("Required.");
            valid = false;
        } else {
            mRecruiterEmail.setError(null);
        }

        String password = mPassword.getText().toString();
        if (TextUtils.isEmpty(password)) {
            mPassword.setError("Required.");
            valid = false;
        } else {
            mPassword.setError(null);
        }

        return valid;
    }

    private void createAccount(String email, String password) {
        Log.d(TAG, "createAccount:" + email);
        if (!validateForm()) {
            return;
        }

        showProgressDialog();

        // [START create_user_with_email]
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener((Activity)getContext(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            Toast.makeText(getContext(), "User Added", Toast.LENGTH_SHORT).show();

                            writeNewCompanyUser(user.getUid(),
                                    mCompanyName.getText().toString(),
                                    mRecruiterEmail.getText().toString(),
                                    mCompanyId.getText().toString(),
                                    mRecruiterName.getText().toString());

                            mListener.onLoadODCActivity();
                            //updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(getContext(), "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            //updateUI(null);
                        }

                        // [START_EXCLUDE]
                        hideProgressDialog();
                        // [END_EXCLUDE]
                    }
                });
        // [END create_user_with_email]
    }

    private void writeNewCompanyUser(String userId, String companyName, String recruiterEmail, String companyId, String recruiterName) {
        Bitmap image = BitmapFactory.decodeResource(getResources(), R.drawable.company_avatar);


        Company company = new Company();
        company.setCompanyid(companyId);
        company.setCompanyname(companyName);
        company.setRecruitername(recruiterName);
        company.setEmail(recruiterEmail);
        company.setLogourl(companyName+"_"+companyId);

        mDatabase.child(users_company_node).child(userId).setValue(company);
        writeImageToStorage(image,companyName+"_"+companyId);
    }

    private void writeImageToStorage(Bitmap image, String node){
        companyLogoStorageRef = storage.getReference();

        StorageReference pImageRef = companyLogoStorageRef.child(companylogos_node+"/"+node);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();

        UploadTask uploadTask = pImageRef.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                Log.d(TAG,"Image stored to storage: "+downloadUrl.toString());
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.register_button:
                createAccount(mRecruiterEmail.getText().toString(), mPassword.getText().toString());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ((CompanyLoginActivity)getActivity()).getSupportActionBar().hide();
    }

    @Override
    public void onStop() {
        super.onStop();
        ((CompanyLoginActivity)getActivity()).getSupportActionBar().show();
    }
}
