package com.example.nikhil.orangedrive.Fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.nikhil.orangedrive.Activity.OrangeDriveCompanyActivity;
import com.example.nikhil.orangedrive.Activity.OrangeDriveStudentActivity;
import com.example.nikhil.orangedrive.Interface.AddJobListener;
import com.example.nikhil.orangedrive.Interface.JobDetailViewListener;
import com.example.nikhil.orangedrive.Models.Company;
import com.example.nikhil.orangedrive.Models.Jobs;
import com.example.nikhil.orangedrive.Models.User;
import com.example.nikhil.orangedrive.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Nikhil on 11/30/2017.
 */

public class AddJobFragment extends Fragment implements AdapterView.OnItemSelectedListener,
        View.OnClickListener{

    AddJobListener mListener;

    EditText mBasicQualifications;
    EditText mCompanyId;
    EditText mCompanyName;
    EditText mJobDescription;
    EditText mJobId;
    EditText mJobTitle;
    EditText mPreferredQualifications;
    EditText mSalaryRange;
    EditText mSeniorityLevel;
    Spinner mJobLocation;
    Button mAddJob;

    String location=null;
    String joblogourl=null;
    String companyId=null;
    String companyName=null;
    public ProgressDialog mProgressDialog;

    public static String company_node="Companies";
    public static String job_node="Jobs";
    private DatabaseReference mCompanyReference;
    private ValueEventListener mCompanyListener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        mCompanyReference= FirebaseDatabase.getInstance().getReference()
                .child(company_node).child(FirebaseAuth.getInstance().getCurrentUser().getUid());
    }

    public AddJobFragment() {}

    public static AddJobFragment newInstance(){
        return new AddJobFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mRootView = inflater.inflate(R.layout.fragment_addjob,container,false);

        try{
            mListener=(AddJobListener) getContext();
        }catch(ClassCastException e){
            throw new ClassCastException("The hosting activity doesn't implement the interface");
        }

        Toolbar toolbar = mRootView.findViewById(R.id.flexible_example_toolbar);
        toolbar.setNavigationOnClickListener(new Toolbar.OnClickListener(){

            @Override
            public void onClick(View v) {
                mListener.backPressed();
            }

        });

         mJobLocation = mRootView.findViewById(R.id.add_location);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.location_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Apply the adapter to the spinner
        mJobLocation.setAdapter(adapter);

        mJobLocation.setOnItemSelectedListener(this);

        //Views
        mBasicQualifications = mRootView.findViewById(R.id.add_basicqualifications);
        mCompanyId = mRootView.findViewById(R.id.add_companyid);
        mCompanyName = mRootView.findViewById(R.id.add_companyname);
        mJobDescription = mRootView.findViewById(R.id.add_jobdesc);
        mJobId = mRootView.findViewById(R.id.add_jobid);
        mJobTitle = mRootView.findViewById(R.id.add_jobtitle);
        mSalaryRange = mRootView.findViewById(R.id.add_salaryrange);
        mSeniorityLevel = mRootView.findViewById(R.id.add_senioritylevel);
        mPreferredQualifications = mRootView.findViewById(R.id.add_preferredqualifications);

        //button
        mAddJob = mRootView.findViewById(R.id.addjob_button);
        mAddJob.setOnClickListener(this);

        return mRootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((OrangeDriveCompanyActivity)getActivity()).getSupportActionBar().hide();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        location = (String)parent.getItemAtPosition(position);
        Log.d("AddJobFragment",location);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }



    @Override
    public void onStart() {
        super.onStart();

        ValueEventListener companyDetailsListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Company company = dataSnapshot.getValue(Company.class);

                joblogourl=company.getLogourl();
                companyId=company.getCompanyid();
                companyName=company.getCompanyname();

                mCompanyId.setText(companyId);
                mCompanyName.setText(companyName);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        mCompanyReference.addValueEventListener(companyDetailsListener);
        mCompanyListener = companyDetailsListener;
    }

    @Override
    public void onStop() {
        super.onStop();
        ((OrangeDriveCompanyActivity)getActivity()).getSupportActionBar().show();
        if(mCompanyListener != null){
            mCompanyReference.removeEventListener(mCompanyListener);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.addjob_button:
                AddJobToFirebase();
                break;
        }
    }

    public void AddJobToFirebase(){

        showProgressDialog();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Jobs job = new Jobs();
                job.setCompanyId(mCompanyId.getText().toString());
                job.setCompanyName(mCompanyName.getText().toString());
                job.setBasicQualifications(mBasicQualifications.getText().toString());
                job.setJobDescription(mJobDescription.getText().toString());
                job.setJobId(mJobId.getText().toString());
                job.setJobTitle(mJobTitle.getText().toString());
                job.setPreferredQualifications(mPreferredQualifications.getText().toString());
                job.setSalaryRange(mSalaryRange.getText().toString());
                job.setSeniorityLevel(mSeniorityLevel.getText().toString());
                job.setLocation(location);
                job.setJoblogourl(joblogourl);

                Map<String, Object> jobValues = job.toMap();
                Map<String, Object> childUpdates = new HashMap<>();
                DatabaseReference mDatabse = FirebaseDatabase.getInstance().getReference();
                childUpdates.put(job_node+ "/" + job.getCompanyId()+"_"+job.getJobId(), jobValues);
                //mDatabse.updateChildren(childUpdates);

                childUpdates.put(company_node+"/"+FirebaseAuth.getInstance().getCurrentUser().getUid()+
                    "/"+"myjobs/"+job.getCompanyId()+"_"+job.getJobId(), jobValues);
                mDatabse.updateChildren(childUpdates);
//                DatabaseReference abc = FirebaseDatabase.getInstance().getReference()
//                        .child(company_node).child(FirebaseAuth.getInstance().getCurrentUser().getUid());
//                abc.child("myjobs").push().setValue(job.getCompanyId()+"_"+job.getJobId());

                hideProgressDialog();
                mListener.backPressed();
            }
        }, 500);
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setMessage("Adding Job…");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
