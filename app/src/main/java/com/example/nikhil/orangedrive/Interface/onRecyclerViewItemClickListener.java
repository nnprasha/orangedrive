package com.example.nikhil.orangedrive.Interface;

import android.view.View;

import java.util.HashMap;

/**
 * Created by Nikhil on 12/4/2017.
 */

public interface onRecyclerViewItemClickListener {
        void loadMovieDetailFragment(HashMap<String, ?> movie, View v);
}
