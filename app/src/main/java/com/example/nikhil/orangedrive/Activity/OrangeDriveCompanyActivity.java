package com.example.nikhil.orangedrive.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.ChangeBounds;
import android.transition.ChangeImageTransform;
import android.transition.ChangeTransform;
import android.transition.Fade;
import android.transition.TransitionSet;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.nikhil.orangedrive.Fragment.AddJobFragment;
import com.example.nikhil.orangedrive.Fragment.CompanyApplicantsFragment;
import com.example.nikhil.orangedrive.Fragment.CompanyJobPostingRecyclerViewFragment;
import com.example.nikhil.orangedrive.Fragment.JobDetailViewFragment;
import com.example.nikhil.orangedrive.Fragment.MyApplicantDetailViewFragment;
import com.example.nikhil.orangedrive.Interface.AddJobListener;
import com.example.nikhil.orangedrive.Interface.CompanyApplicantsListener;
import com.example.nikhil.orangedrive.Interface.CompanyJobsRecyclerViewListener;
import com.example.nikhil.orangedrive.Interface.JobDetailViewListener;
import com.example.nikhil.orangedrive.Interface.MyApplicantsDetailViewListener;
import com.example.nikhil.orangedrive.Models.Company;
import com.example.nikhil.orangedrive.Models.User;
import com.example.nikhil.orangedrive.R;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.HashMap;

/**
 * Created by Nikhil on 11/30/2017.
 */

public class OrangeDriveCompanyActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        AddJobListener, CompanyJobsRecyclerViewListener, CompanyApplicantsListener, JobDetailViewListener,
        MyApplicantsDetailViewListener{
    Toolbar toolbar;
    NavigationView navigationView;
    DrawerLayout drawerLayout;
    public ProgressDialog mProgressDialog;

    ValueEventListener mUserInfo;
    DatabaseReference mRef;
    ImageView mUserProfile;
    TextView mUserName;
    TextView mUserEmail;
    private FirebaseStorage storage;
    StorageReference userProfileImageStorage;
    public static String companylogos="companylogos/";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orangedrivecompanyactivity);

        toolbar=(Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        storage = FirebaseStorage.getInstance();

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setTitleTextColor(getColor(R.color.colorWhite));

        navigationView=(NavigationView) findViewById(R.id.navigationview);
        navigationView.setNavigationItemSelectedListener(this);

        drawerLayout=(DrawerLayout) findViewById(R.id.drawer);
        ActionBarDrawerToggle actionBarDrawerToggle=
                new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.DrawerOpen,R.string.DrawerClose){
                    @Override
                    public void onDrawerClosed(View drawerView) {
                        super.onDrawerClosed(drawerView);
                    }

                    @Override
                    public void onDrawerOpened(View drawerView) {
                        super.onDrawerOpened(drawerView);
                    }
                };

        drawerLayout.setDrawerListener(actionBarDrawerToggle);

        actionBarDrawerToggle.syncState();


        mUserProfile = navigationView.getHeaderView(0).findViewById(R.id.ods_profileimage);
        mUserEmail = navigationView.getHeaderView(0).findViewById(R.id.ods_headeremail);
        mUserName = navigationView.getHeaderView(0).findViewById(R.id.ods_headername);

        mRef = FirebaseDatabase.getInstance().getReference()
                .child("Companies")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .getRef();

        ValueEventListener userInfo = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Company company = dataSnapshot.getValue(Company.class);

                mUserName.setText(company.getRecruitername());

                mUserEmail.setText(company.getEmail());

                userProfileImageStorage = storage.getReference().child(companylogos+company.getLogourl());

                Glide.with(getApplicationContext())
                        .using(new FirebaseImageLoader())
                        .load(userProfileImageStorage)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .into(mUserProfile);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        mRef.addValueEventListener(userInfo);
        mUserInfo = userInfo;
        if(savedInstanceState == null)
        {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.main_container, CompanyApplicantsFragment.newInstance())
                    .addToBackStack(null)
                    .commit();
        }

    }

    @Override
    protected void onStop() {
        super.onStop();

        if(mUserInfo!=null){
            mRef.removeEventListener(mUserInfo);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_signout,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.icon_signout:
                buildSignoutDialog().show();
                return true;
            case R.id.home:
                onBackPressed();
                return true;
        }
        return false;
    }

    public Dialog buildSignoutDialog(){
        // 1. Instantiate an AlertDialog.Builder with its constructor
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        // 2. Chain together various setter methods to set the dialog characteristics
        builder.setMessage("Are you sure you want to Signout?")
                .setTitle("SIGNOUT");

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked yes button
                showProgressDialog();
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        FirebaseAuth.getInstance().signOut();
                        hideProgressDialog();
                        Intent intent=new Intent(OrangeDriveCompanyActivity.this, MainActivity.class);
                        startActivity(intent);
                    }
                }, 500);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
                dialog.cancel();
            }
        });
        // 3. Get the AlertDialog from create()
        AlertDialog dialog = builder.create();
        return dialog;
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Signing out…");
            mProgressDialog.setIndeterminate(true);
        }
        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }



    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Intent intent;
        switch (item.getItemId()){
            case R.id.icon_my_applicants:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.main_container, CompanyApplicantsFragment.newInstance())
                        .addToBackStack(null)
                        .commit();
                break;
            case R.id.icon_company_profile:
                intent=new Intent(OrangeDriveCompanyActivity.this, CompanyProfileActivity.class);
                startActivity(intent);
                break;
            case R.id.icon_add_job:
                    getSupportFragmentManager().beginTransaction()
                    .replace(R.id.main_container, AddJobFragment.newInstance())
                    .addToBackStack(null)
                    .commit();
                break;
            case R.id.icon_my_jobs:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.main_container, CompanyJobPostingRecyclerViewFragment.newInstance())
                        .addToBackStack(null)
                        .commit();
                break;
        }

        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void backPressed() {
        onBackPressed();
    }

    @Override
    public void setToolBarTitle(String name) {
        toolbar.setTitle(name);
    }

    @Override
    public void loadMovieDetailFragment(HashMap<String, ?> movie, View v) {
        JobDetailViewFragment newFragment = JobDetailViewFragment.newInstance(movie, true);
        Fragment oldFragment = getSupportFragmentManager().findFragmentById(R.id.main_container);

        oldFragment.setExitTransition(new Fade());
        oldFragment.setSharedElementReturnTransition(new DetailsTransition());

        newFragment.setEnterTransition(new Fade());
        newFragment.setSharedElementEnterTransition(new DetailsTransition());

        getSupportFragmentManager().beginTransaction()
                .addSharedElement(v,v.getTransitionName())
                .replace(R.id.main_container, newFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void setToolbarTitle(String name) {
        toolbar.setTitle(name);
    }

    @Override
    public void onLoadApplicantsDetailView(HashMap<String, ?> map, View v) {
        MyApplicantDetailViewFragment newFragment = MyApplicantDetailViewFragment.newInstance(map);
        Fragment oldFragment = getSupportFragmentManager().findFragmentById(R.id.main_container);

        oldFragment.setExitTransition(new Fade());
        oldFragment.setSharedElementReturnTransition(new DetailsTransition());

        newFragment.setEnterTransition(new Fade());
        newFragment.setSharedElementEnterTransition(new DetailsTransition());

        getSupportFragmentManager().beginTransaction()
                .addSharedElement(v,v.getTransitionName())
                .replace(R.id.main_container, newFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void changeToolBarTitle(String name) {
        toolbar.setTitle(name);
    }

    @Override
    public void pressedBackButton() {
        onBackPressed();
    }

    public class DetailsTransition extends TransitionSet {
        public DetailsTransition() {
            setOrdering(ORDERING_TOGETHER);
            addTransition(new ChangeBounds())
                    .addTransition(new ChangeTransform())
                    .addTransition(new ChangeImageTransform());
        }
    }
}
