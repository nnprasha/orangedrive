package com.example.nikhil.orangedrive.Interface;

/**
 * Created by Nikhil on 11/27/2017.
 */

public interface StudentProfileListener {
    void changeProfileNameAndImage(String name, String pImageName);
}
