package com.example.nikhil.orangedrive.Interface;

/**
 * Created by Nikhil on 12/1/2017.
 */

public interface CompanyProfileEditListener {
    void setToolBarTitle(String name);
    void updateSuccessful();
}
