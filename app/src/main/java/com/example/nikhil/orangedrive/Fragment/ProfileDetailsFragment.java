package com.example.nikhil.orangedrive.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.nikhil.orangedrive.Interface.StudentLoginFragmentListener;
import com.example.nikhil.orangedrive.Interface.StudentProfileListener;
import com.example.nikhil.orangedrive.Models.User;
import com.example.nikhil.orangedrive.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * Created by Nikhil on 11/27/2017.
 */

public class ProfileDetailsFragment extends Fragment {

    TextView mName;
    TextView mNetId;
    TextView mAddress;
    TextView mBday;
    TextView mCgpa;
    TextView mPhno;

    private DatabaseReference mUsersReference;
    private static final String users_node="Students";
    private ValueEventListener mUserListener;
    StudentProfileListener mListener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        //initialize db
        mUsersReference= FirebaseDatabase.getInstance().getReference()
                .child(users_node).child(FirebaseAuth.getInstance().getCurrentUser().getUid());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mRootView = inflater.inflate(R.layout.fragment_profiledetails, container, false);

        try{
            mListener=(StudentProfileListener) getContext();
        }catch(ClassCastException e){
            throw new ClassCastException("The hosting activity doesn't implement the interface");
        }

        //views
        mName = mRootView.findViewById(R.id.name_value);
        mAddress = mRootView.findViewById(R.id.address_value);
        mCgpa = mRootView.findViewById(R.id.cgpa_value);
        mBday = mRootView.findViewById(R.id.bday_value);
        mPhno = mRootView.findViewById(R.id.phno_value);
        mNetId = mRootView.findViewById(R.id.netid_value);

        return mRootView;
    }

    @Override
    public void onStart() {
        super.onStart();

        ValueEventListener userDetailsListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);

                mNetId.setText(user.netid);
                mPhno.setText(user.phno);
                mCgpa.setText(user.cgpa);
                mBday.setText(user.bday);
                mName.setText(user.name);
                mAddress.setText(user.address);

                mListener.changeProfileNameAndImage(user.name, user.profileurl);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        mUsersReference.addValueEventListener(userDetailsListener);
        mUserListener = userDetailsListener;
    }

    @Override
    public void onStop() {
        super.onStop();

        if(mUserListener != null){
            mUsersReference.removeEventListener(mUserListener);
        }
    }

    public static Fragment newInstance() {
        return new ProfileDetailsFragment();
    }
}
