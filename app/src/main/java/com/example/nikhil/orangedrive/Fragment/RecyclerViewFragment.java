package com.example.nikhil.orangedrive.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.nikhil.orangedrive.Adapter.MyFirebaseRecyclerAdapter;
import com.example.nikhil.orangedrive.Data.JobsData;
import com.example.nikhil.orangedrive.Interface.StudentJobApplicationsListener;
import com.example.nikhil.orangedrive.Interface.onRecyclerViewItemClickListener;
import com.example.nikhil.orangedrive.Models.Jobs;
import com.example.nikhil.orangedrive.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.FadeInLeftAnimator;
import jp.wasabeef.recyclerview.animators.FlipInBottomXAnimator;
import jp.wasabeef.recyclerview.animators.FlipInLeftYAnimator;

/**
 * Created by Nikhil on 11/29/2017.
 */

/**
 * Created by Nikhil on 9/26/2017.
 */

public class RecyclerViewFragment extends Fragment {

    StudentJobApplicationsListener mListener2;
    RecyclerView mRecyclerView;
    RecyclerView.LayoutManager mLayoutManager;
    ImageView signout;
    //MyRecyclerViewAdapter mRecyclerViewAdapter;
    SwipeRefreshLayout mSwipeRefreshLayout;

    MyFirebaseRecyclerAdapter myFirebaseRecyclerAdapter;

    JobsData movieData;
    public static RecyclerViewFragment newInstance()
    {
        RecyclerViewFragment fragment = new RecyclerViewFragment();
        return fragment;
    }

    public RecyclerViewFragment(){}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final onRecyclerViewItemClickListener mListener;

        //((MainActivity)getActivity()).setActionBarTitle("Movie List");

        try{
            mListener2 = (StudentJobApplicationsListener) getContext();
            mListener=(onRecyclerViewItemClickListener) getContext();
        }catch(ClassCastException e){
            throw new ClassCastException("The hosting activity doesn't implement the interface");
        }
        mListener2.setToolbarTitle("Job Listing");
        DatabaseReference childRef= FirebaseDatabase.getInstance().getReference().child("Jobs").getRef();
        movieData=new JobsData(childRef);

        final View rootView=inflater.inflate(R.layout.fragment_recyclerview,container,false);

        mRecyclerView = rootView.findViewById(R.id.cardList);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager=new LinearLayoutManager(getActivity());

        mRecyclerView.setLayoutManager(mLayoutManager);

        //DatabaseReference childRef= FirebaseDatabase.getInstance().getReference().child("Jobs").getRef();
        myFirebaseRecyclerAdapter=new MyFirebaseRecyclerAdapter(Jobs.class, R.layout.layout_cardview,
                MyFirebaseRecyclerAdapter.MovieViewHolder.class, childRef, getContext());

        //Set adapter animation
        //mRecyclerView.setAdapter(new ScaleInAnimationAdapter(myFirebaseRecyclerAdapter));


        ScaleInAnimationAdapter alphaAdapter = new ScaleInAnimationAdapter(myFirebaseRecyclerAdapter);
        alphaAdapter.setDuration(500);
        mRecyclerView.setAdapter(alphaAdapter);

        //mRecyclerView.setAdapter(new AlphaInAnimationAdapter(myFirebaseRecyclerAdapter));
        if (movieData.getSize() == 0) {
            movieData.setAdapter(myFirebaseRecyclerAdapter);
            //getApplicationContext()-activity is used movieData.initializeDataFromCloud();
            movieData.setContext(getActivity());
            movieData.initializeDataFromCloud();
        }
        //defaultAnimation();

        //setting item animation
        itemAnimation();

        myFirebaseRecyclerAdapter.setOnItemClickListener(new MyFirebaseRecyclerAdapter.OnRecyclerViewItemClickListener(){

            @Override
            public void OnItemClick(final View v, int position) {
                HashMap<String, ?> movie = (HashMap<String, ?>) movieData.getItem(position);
                String id = (String) movie.get("CompanyId") + "_" + movie.get("JobId");
                DatabaseReference ref = movieData.getFireVaseRef();
                ref.child(id).addListenerForSingleValueEvent(new com.google.firebase.database.ValueEventListener(){

                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        HashMap<String, String> movie =  (HashMap<String, String>) dataSnapshot.getValue();
                        mListener.loadMovieDetailFragment(movie, v.findViewById(R.id.cardImage));
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.d("My Test", "The read failed: " + databaseError.getMessage());
                    }
                });
            }
        });
        mSwipeRefreshLayout = rootView.findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                movieData.initializeDataFromCloud();
                myFirebaseRecyclerAdapter.notifyDataSetChanged();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
        return rootView;
    }
    public void defaultAnimation()
    {
        DefaultItemAnimator animator=new DefaultItemAnimator();
        animator.setAddDuration(100);
        animator.setRemoveDuration(100);
        mRecyclerView.setItemAnimator(animator);
    }

    public void itemAnimation()
    {
        FlipInBottomXAnimator animator = new FlipInBottomXAnimator();
        animator.setAddDuration(500);
        mRecyclerView.setItemAnimator(animator);
//        FadeInLeftAnimator animator=new FadeInLeftAnimator();
//        animator.setAddDuration(100);
//        animator.setRemoveDuration(100);
//        mRecyclerView.setItemAnimator(animator);

    }



}

