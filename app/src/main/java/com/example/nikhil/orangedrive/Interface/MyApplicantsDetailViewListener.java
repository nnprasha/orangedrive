package com.example.nikhil.orangedrive.Interface;

/**
 * Created by Nikhil on 12/5/2017.
 */

public interface MyApplicantsDetailViewListener {
    void changeToolBarTitle(String name);
    void pressedBackButton();
}
