package com.example.nikhil.orangedrive.Activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.example.nikhil.orangedrive.Fragment.CompanyProfileEditFragment;
import com.example.nikhil.orangedrive.Fragment.ProfileEditFragment;
import com.example.nikhil.orangedrive.Interface.CompanyProfileEditListener;
import com.example.nikhil.orangedrive.R;

/**
 * Created by Nikhil on 12/1/2017.
 */

public class CompanyProfileEditActivity extends AppCompatActivity implements CompanyProfileEditListener {


    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_companyprofileedit);

        setUpToolBar();

        if(savedInstanceState == null)
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.editprofilecontainer, CompanyProfileEditFragment.newInstance())
                    .commit();
    }

    private void setUpToolBar(){
        toolbar=(Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setTitleTextColor(getColor(R.color.colorWhite));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setToolBarTitle(String name) {
        toolbar.setTitle(name);
        toolbar.setTitleTextColor(getColor(R.color.colorWhite));
    }

    @Override
    public void updateSuccessful() {
        onBackPressed();
    }
}
