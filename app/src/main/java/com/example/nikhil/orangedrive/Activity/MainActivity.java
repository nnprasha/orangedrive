package com.example.nikhil.orangedrive.Activity;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.nikhil.orangedrive.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener{

    Toolbar toolbar;
    NavigationView navigationView;
    DrawerLayout drawerLayout;
    TextView toolBarTitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        toolbar=(Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setTitle("Home");
        toolbar.setTitleTextColor(getColor(R.color.colorWhite));

        navigationView=(NavigationView) findViewById(R.id.navigationview);
        navigationView.setNavigationItemSelectedListener(this);

        drawerLayout=(DrawerLayout) findViewById(R.id.drawer);
        ActionBarDrawerToggle actionBarDrawerToggle=
                new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.DrawerOpen,R.string.DrawerClose){
                    @Override
                    public void onDrawerClosed(View drawerView) {
                        super.onDrawerClosed(drawerView);
                    }

                    @Override
                    public void onDrawerOpened(View drawerView) {
                        super.onDrawerOpened(drawerView);
                    }
                };

        drawerLayout.setDrawerListener(actionBarDrawerToggle);

        actionBarDrawerToggle.syncState();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        Intent intent;
        switch(item.getItemId()){
            case R.id.icon_student_login:
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if (user != null) {
                    // User is signed in
                    intent=new Intent(MainActivity.this, OrangeDriveStudentActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.from_middle, R.anim.to_middle);

                } else {
                    // No user is signed in
                    intent=new Intent(MainActivity.this, StudentLoginActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.from_middle, R.anim.to_middle);

                }
                break;
            case R.id.icon_company_login:
                FirebaseUser cuser = FirebaseAuth.getInstance().getCurrentUser();
                if (cuser != null) {
                    // User is signed in
                    intent=new Intent(MainActivity.this, OrangeDriveStudentActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.from_middle, R.anim.to_middle);

                } else {
                    // No user is signed in
                    intent=new Intent(MainActivity.this, CompanyLoginActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.from_middle, R.anim.to_middle);

                }
                break;
            case R.id.icon_weather:
                startActivity(new Intent(MainActivity.this, WeatherActivity.class));
                overridePendingTransition(R.anim.from_middle, R.anim.to_middle);
                break;
        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }
}
