package com.example.nikhil.orangedrive.Interface;

/**
 * Created by Nikhil on 12/1/2017.
 */

public interface CompanyProfileListener {
    void changeProfileNameAndImage(String name, String pImageName);
}
