package com.example.nikhil.orangedrive.Interface;

/**
 * Created by Nikhil on 11/30/2017.
 */

public interface CompanyLoginFragmentListener {
        void setToolBarTitle(String name);
        void onLoadRegisterFragment();
        void onLoadODCActivity();
        void backPressed();
}
